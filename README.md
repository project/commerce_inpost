# Commerce InPost

This module allows to use of InPost parcel lockers shipment (see more: `https://inpost.pl/`)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/commerce_inpost).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/commerce_inpost).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module adds InPost shipping provider to Drupal commerce,
so it requires commerce and commerce_shipping modules.
You need to download/install them first in order to use this module.


## Installation

Install normally as other modules are installed. For Support:
https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules


## Configuration

1. You need to configure commerce_shipping module first.

2. Go to `admin/commerce/shipping-methods` and create a shipping method
   which use the InPost Shipping plugin.

3. Go to `admin/commerce/config/checkout-flows` and edit your checkout flow, ex:
   `/admin/commerce/config/checkout-flows/manage/shipping`
   By default InPost is placed under the "Order Information", you can
   customize it by drag&drop to the preferred section.


## Maintainers

- Grzegorz Pietrzak - [gpietrzak](https://www.drupal.org/u/gpietrzak)
- tmakiel - [tmakiel](https://www.drupal.org/u/tmakiel)
- Piotr Kamieniecki - [piotr.kamieniecki](https://www.drupal.org/u/piotrkamieniecki)
- Mariusz Andrzejewski - [sayco](https://www.drupal.org/u/sayco)
