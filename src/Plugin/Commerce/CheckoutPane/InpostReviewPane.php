<?php

namespace Drupal\commerce_inpost\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Add InPost review pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_inpost_review_pane",
 *   label = @Translation("InPost review"),
 *   default_step = "review",
 *   wrapper_element = "fieldset",
 * )
 */
class InpostReviewPane extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $data = $this->order->getData('inpost_point');
    if (!empty($data)) {
      $pane_form['inpost_information'] = [
        '#theme' => 'commerce_inpost_review',
        '#information' => $data,
      ];
    }

    return $pane_form;
  }

}
