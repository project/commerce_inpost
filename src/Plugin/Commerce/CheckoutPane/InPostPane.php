<?php

namespace Drupal\commerce_inpost\Plugin\Commerce\CheckoutPane;

use Drupal\address\AddressInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_inpost\Plugin\Commerce\ShippingMethod\InPostShipping;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\Profile;
use Drupal\Core\Language\LanguageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add InPost Pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_inpost_checkout_pane",
 *   label = @Translation("InPost"),
 *   default_step = "order_information",
 *   wrapper_element = "fieldset",
 * )
 */
class InPostPane extends CheckoutPaneBase {

  /**
   * EntityFieldManager instance.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, EntityFieldManager $entity_field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'custom_number' => FALSE,
        'phone_number_field' => '_empty',
        'entity_type' => 'profile',
      ] + parent::defaultConfiguration();
  }


  /**
   * Add hidden fields for pane form to collect InPost point data.
   *
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $ids = $this->getInpostIds();
    if (count($ids) > 0) {
      $inpost_point = $this->order->getData('inpost_point', []);
      $conditions = $this->generateConditions($ids);
      $pane_form['commerce_inpost_wrapper'] = [
        '#type' => 'fieldset',
        '#weight' => 5,
        '#title' => t('Pick up point information'),
        '#states' => [
          'visible' => [
            ':input[name="shipping_information[shipments][0][shipping_method][0]"]' => $conditions,
          ],
        ],
        'commerce_inpost_point_information' => [
          '#type' => 'container',
          '#attributes' => [
            'id' => 'commerce-inpost-point-information',
          ],
          'commerce_inpost_point_name' => [
            '#type' => 'markup',
            '#markup' => '<div class="pickup-point-information">' . $inpost_point['name'] . '<br/>'
              . $inpost_point['address_line1'] . '<br/>'
              . $inpost_point['address_line2'] . '</div>',
            '#access' => !empty($inpost_point),
          ],
        ],
        'commerce_inpost_change' => [
          '#type' => 'button',
          '#value' => $this->t('Select pick up point'),
          '#ajax' => [
            'callback' => [$this, 'selectPickupPoint'],
          ],
          '#attributes' => [
            'class' => [
              'inpost-change-button',
            ],
          ],
        ],
        'commerce_inpost_point_name' => [
          '#type' => 'hidden',
          '#attributes' => ['id' => 'commerce-inpost-point-name'],
          '#default_value' => $inpost_point['name'] ?? '',
        ],
        'commerce_inpost_point_address_line1' => [
          '#type' => 'hidden',
          '#attributes' => ['id' => 'commerce-inpost-point-address-line1'],
          '#default_value' => $inpost_point['address_line1'] ?? '',
        ],
        'commerce_inpost_point_address_line2' => [
          '#type' => 'hidden',
          '#attributes' => ['id' => 'commerce-inpost-point-address-line2'],
          '#default_value' => $inpost_point['address_line2'] ?? '',
        ],
      ];
      $phoneNumber = '';
      $numberField = $this->configuration['phone_number_field'];
      if ($this->configuration['entity_type'] == 'user') {

        $customer = $this->order->getCustomer();
        if (!empty($customer) && $customer->hasField($numberField)) {
          $phoneNumber = $customer->get($numberField)->value;
        }
      }
      if ($this->configuration['entity_type'] == 'profile') {
        $profile = $this->getShippingProfile();
        if (!empty($profile) && $profile->hasField($numberField)) {
          $phoneNumber = $profile->get($numberField)->value;
        }
      }

      if (!empty($inpost_point['phone_number'])) {
        $phoneNumber = $inpost_point['phone_number'];
      }
      $pane_form['commerce_inpost_wrapper']['phone_number'] = [
        '#type' => 'textfield',
        '#title' => t('Delivery phone number'),
        '#length' => 10,
        '#default_value' => $phoneNumber,
      ];

      $pane_form['#attached']['library'][] = 'commerce_inpost/commerce_inpost.geowidget';
      $pane_form['#title'] = '';

      $this->alterForm($complete_form);
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['phone_number_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Phone number field settings'),
      '#description' => $this->t('Setup custom phone number field or use one provided by the module'),
    ];
    $form['phone_number_settings']['custom_number'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I have phone number field in customer profile set up already'),
      '#description' => $this->t('If checked module will use your profile field to use with delivery.'),
      '#default_value' => $this->configuration['custom_number'],
    ];
    $options = $this->getOptions();
    $default = $this->configuration['entity_type'] . '.' . $this->configuration['phone_number_field'];
    $form['phone_number_settings']['phone_number_field'] = [
      '#type' => 'select',
      '#title' => t('Profile field containing phone number'),
      '#options' => $options,
      '#default_value' => empty($default) ? reset($options) : $default,
      '#attributes' => [
        'id' => 'custom-phone-field',
      ],
      '#states' => [
        'visible' => [
          ':input[name="configuration[panes][commerce_inpost_checkout_pane][configuration][phone_number_settings][custom_number]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="configuration[panes][commerce_inpost_checkout_pane][configuration][phone_number_settings][custom_number]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['custom_number'] = !empty($values['phone_number_settings']['custom_number']);
      $phoneField = explode('.', $values['phone_number_settings']['phone_number_field']);
      $this->configuration['entity_type'] = $phoneField[0];
      $this->configuration['phone_number_field'] = $phoneField[1];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationSummary() {
    if (!empty($this->configuration['custom_number'])) {
      $summary = "{$this->t('Use custom phone number field: YES')}<br>";
      $summary .= "{$this->t('Entity')}: {$this->configuration['entity_type']} <br>";
      $summary .= "{$this->t('Using field')}: {$this->configuration['phone_number_field']}<br>";
    }
    else {
      $summary = $this->t('Use custom phone number field: NO') . '<br>';
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#type'] == 'submit') {
      $values = $form_state->getValues();
      $ids = $this->getInpostIds();
      foreach ($values['shipping_information']['shipments'] as $shipment) {
        if (in_array($shipment['shipping_method'][0], $ids)) {
          $wrapperValues = $values['commerce_inpost_checkout_pane']['commerce_inpost_wrapper'];

          unset($wrapperValues['commerce_inpost_change']);
          if (empty($wrapperValues['phone_number'])) {
            $form_state->setErrorByName('phone_number', $this->t('Please provide a valid phone number'));
          }
          unset($wrapperValues['phone_number']);
          foreach ($wrapperValues as $key => $value) {
            if (empty($value)) {
              $form_state->setErrorByName($key, $this->t('Please select a pickup point'));
            }
          }

        }
      }
    }
  }

  /**
   * Save InPost point name and address.
   *
   * @inheritDoc
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValues();
    $inpost_point = $form_state->getValue($pane_form['#parents']);
    $ids = $this->getInpostIds();
    if (isset($values['shipping_information']['shipments'][0]['shipping_method'][0])) {
      $shipment_method = $values['shipping_information']['shipments'][0]['shipping_method'][0];
      if (isset($inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_name'])
        && in_array($shipment_method, $ids)) {
        $orderData = [
          'name' => $inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_name'],
          'address_line1' => $inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_address_line1'],
          'address_line2' => $inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_address_line2'],
          'phone_number' => $inpost_point['commerce_inpost_wrapper']['phone_number'],
        ];
        $this->order->setData('inpost_point', $orderData);
      }
      parent::submitPaneForm($pane_form, $form_state, $complete_form);
    }
  }

  /**
   * Create shipping profile based on the point address.
   *
   * @param array $inpost_point
   *   InPost point address.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setShippingProfile(array $inpost_point) {
    $shipments = $this->order->get('shipments')->referencedEntities();
    if (isset($shipments[0])) {
      $address = $shipments[0]->getShippingProfile()
        ->get('address')
        ->first();
      $profile = Profile::create([
        'type' => 'customer',
        'uid' => $this->order->getCustomerId(),
        'address' => $this->shippingAddress($inpost_point, $address),
        'data' => [
          'copy_to_address_book' => FALSE,
        ],
      ]);
      $profile->save();
      $shipments = $this->order->get('shipments')->referencedEntities();
      $shipments[0]->setShippingProfile($profile);
      $shipments[0]->save();
    }
  }

  /**
   * Get shipping address from InPost point.
   *
   * @param array $inpost_point
   *   InPost point address.
   * @param \Drupal\address\AddressInterface $address
   *   Shipping address Item.
   *
   * @return array
   *   Shipping address for order.
   */
  public function shippingAddress(array $inpost_point, AddressInterface $address) {
    $language = \Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);

    $postal = $inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_address_line2'];

    return [
      'country_code' => $address->getCountryCode(),
      'langcode' => $language->getId(),
      'given_name' => $address->getGivenName(),
      'family_name' => $address->getFamilyName(),
      'organization' => '',
      'address_line1' => $inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_address_line1'],
      'address_line2' => t('InPost parcel lockers') . ' ' . $inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_name'],
      'postal_code' => strlen($postal) > 8 ? substr($postal, 0, 6) : '',
      'locality' => strlen($postal) > 8 ? substr($postal, 7) : '',
      'additional_name' => $inpost_point['commerce_inpost_wrapper']['commerce_inpost_point_name'],
      'sorting_code' => NULL,
      'dependent_locality' => NULL,
      'administrative_area' => NULL,
    ];
  }

  /**
   * Ajax callback for displaying modal with point selection.
   *
   * @param array $form
   *   Current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajax response with invoke command.
   */
  public function selectPickupPoint(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $ids = $this->getInpostIds();
    $triggeringElement = $form_state->getTriggeringElement();
    if (in_array($triggeringElement['#value'], $ids) || $triggeringElement['#type'] == 'button') {
      $response->addCommand(new InvokeCommand('body', 'modal', []));
    }

    return $response;
  }

  /**
   * Alters shipping method form to add ajax callback on method selection.
   *
   * @param array $form
   *   Form to alter.
   */
  public function alterForm(array &$form) {
    $shipments = $form['shipping_information']['shipments'];
    unset($shipments['#type']);
    foreach ($shipments as $key => $shipment) {
      $form['shipping_information']['shipments'][$key]['shipping_method']['widget'][0]['#ajax'] = [
        'callback' => [
          $this,
          'selectPickupPoint',
        ],
        'event' => 'change',
      ];
    }
  }

  /**
   * Returns array of user and profiles fields.
   *
   * @return array
   *   Array of possible fields ['machine_name' => '[EntityType]:
   *   [machine_name]']
   */
  private function getOptions(): array {
    $userFields = $this->entityFieldManager
      ->getFieldStorageDefinitions('user');
    $fields = [];
    foreach ($userFields as $userField) {
      if ($userField instanceof BaseFieldDefinition) {
        continue;
      }
      $name = $userField->label();
      $label = explode('.', $name);
      /** @var \Drupal\field\Entity\FieldStorageConfig $userField */
      $fields[$name] = $label[0] . ': ' . $label[1];
    }
    $profileFields = $this->entityFieldManager
      ->getFieldStorageDefinitions('profile');
    foreach ($profileFields as $profileField) {
      if ($profileField instanceof BaseFieldDefinition) {
        continue;
      }
      $name = $profileField->label();
      $label = explode('.', $name);
      $fields[$name] = $label[0] . ': ' . $label[1];
    }

    return $fields;
  }

  /**
   * Returns inpost shipping methods ids.
   *
   * TODO: Solve the equation for multiple shipments.
   */
  private function getInpostIds() {
    $shipping_methods = $this->entityTypeManager->getStorage('commerce_shipping_method')
      ->loadMultiple();
    $shipments = [];
    /** @var \Drupal\commerce_shipping\Entity\ShippingMethod $shipping_method */
    foreach ($shipping_methods as $shipping_method) {
      if ($shipping_method->getPlugin() instanceof InPostShipping) {
        $shipments[] = $shipping_method->id() . '--in_post';
      }
    }

    return $shipments;
  }

  /**
   * Generates conditions for states.
   *
   * @param array $ids
   *   Ids of inpost shipping methods.
   *
   * @return array
   *   Array of conditions.
   */
  private function generateConditions(array $ids): array {
    $conditions = [];
    foreach ($ids as $id) {
      $conditions[] = ['value' => $id];
    }

    return $conditions;
  }

  /**
   * Gets the shipping profile.
   *
   * The shipping profile is assumed to be the same for all shipments.
   * Therefore, it is taken from the first found shipment, or created from
   * scratch if no shipments were found.
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *   The shipping profile.
   */
  protected function getShippingProfile() {
    $shipping_profile = NULL;
    if (!$this->order->hasField('shipments')) {
      return $shipping_profile;
    }
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    foreach ($this->order->get('shipments')
               ->referencedEntities() as $shipment) {
      $shipping_profile = $shipment->getShippingProfile();
      break;
    }

    return $shipping_profile;
  }

}
