/**
 * @file
 */

(function (Drupal, $) {

  Drupal.behaviors.commerce_inpost = {
    attach: function (context, settings) {

      /**
       * Initialize InPost geowidget.
       */
      function init() {
        if (typeof window.easyPack !== 'undefined') {
          easyPack.init({
            defaultLocale: 'pl',
            mapType: 'osm',
            searchType: 'osm',
            points: {
              types: ['parcel_locker']
            },
            map: {
              initialTypes: ['parcel_locker']
            }
          });
        }
      }

      function generatePointMarkup(point) {
        return `<div class="pickup-point-information">${point.name}<br>${point.address.line1}<br>${point.address.line2}</div>`;
      }

      $.fn.modal = function () {
        init();
        easyPack.modalMap(function (point, modal) {
          modal.closeModal();
          $('#commerce-inpost-point-name').val(point.name);
          $('#commerce-inpost-point-address-line1').val(point.address.line1);
          $('#commerce-inpost-point-address-line2').val(point.address.line2);
          $('#commerce-inpost-point-information').html(
            generatePointMarkup(point)
          );

        }, {width: 700, height: 600});
      };
    }
  };

})(Drupal, jQuery);
